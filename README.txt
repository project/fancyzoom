
This module adds FancyZoom 1.1 to your site. This causes all links that point directly to an image file to zoom the image in-place instead of opening it in a new page. FancyZoom does this automatically by locating the image links on each page, so there's no extra setup required.

FancyZoom works best with thumbnails linked to larger images. It works especially well with the Image Cache module. To disable zooming on an image link, add the rel="nozoom" attribute to the <a> tag.

Configuration options can be found at admin/settings/fancyzoom. The Farbtastic color picker will be used if you have the Colorpicker module installed.


*** GPLv2 LICENSE ***

The FancyZoom Drupal Module is released AS A WHOLE under GPLv2. The included FancyZoom javascript is licensed under the new GPL-compatible BSD license, and is therefore not in conflict with GPLv2. In addition I have received the author's blessing in including his code as permitted by the GPLv2 license.


*** FEE FOR COMMERCIAL USE ***

The FancyZoom Drupal Module is released AS A WHOLE under GPLv2. The included FancyZoom javascript is licensed under the new GPL-compatible BSD license. The author requests a one-time registration fee for its use on commercial or for-profit web sites. Please visit www.fancyzoom.com for more details.
